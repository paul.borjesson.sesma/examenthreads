package com.company;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;


public class Magatzem_peces_Dades {
	// Aquí s'emmagatzemaran totes les peces electròniques que hi ha en les naus i en la drassana. 
	// Totes les naus i la drassana han d'enviar les seves peces a aquest magatzem.
	
	// És una llista inicialitzada per Drassana que contindrà tants false's com naus hi hagi en la drassana.
	private ArrayList <Boolean> nausAcabades = new ArrayList<>();
	
	// La clau serà peça_ID. El valor seran els objecte de tipus Peça_electronica_Dades i es diferenciaran pel peça_num_serie.
	private LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronicaOperatives = new LinkedHashMap<String, LinkedList<Peça_electronica_Dades>>();
	
	// La clau serà peça_ID. El valor seran els objecte de tipus Peça_electronica_Dades i es diferenciaran pel peça_num_serie.
	private LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronicaTrencades = new LinkedHashMap<String, LinkedList<Peça_electronica_Dades>>();

	private Semaphore semaphore4Entry = new Semaphore(4);
	

	public ArrayList<Boolean> getNausAcabades() {
		return nausAcabades;
	}

	public LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronicaOperatives() {
		return mapaPecesElectronicaOperatives;
	}

	public void setMapaPecesElectronicaOperatives(LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica) {
		this.mapaPecesElectronicaOperatives = mapaPecesElectronica;
	}

	public LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronicaTrencades() {
		return mapaPecesElectronicaTrencades;
	}

	public void setMapaPecesElectronicaTrencades(
			LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronicaTrencades) {
		this.mapaPecesElectronicaTrencades = mapaPecesElectronicaTrencades;
	}


	
	
	
	public void notificaNauAcabada() {
		for (int i = 0; i < nausAcabades.size(); i++) {
			if (!nausAcabades.get(i)) {
				nausAcabades.set(i, true);
				break;
			}
		}
	}

	
	public void processarPecesElectronica(Peça_electronica_Dades peçaRebuda) {
		try {
			semaphore4Entry.acquire();
			if (peçaRebuda.isPeça_trencada()) {
				Thread.sleep(5000);
				if (mapaPecesElectronicaTrencades.keySet().contains(peçaRebuda.getPeça_ID())) {
					mapaPecesElectronicaTrencades.get(peçaRebuda.getPeça_ID()).add(peçaRebuda);
				} else {
					LinkedList<Peça_electronica_Dades> newList = new LinkedList<>();
					newList.add(peçaRebuda);
					mapaPecesElectronicaTrencades.put(peçaRebuda.getPeça_ID(), newList);
				}
			} else {
				if (mapaPecesElectronicaOperatives.keySet().contains(peçaRebuda.getPeça_ID())) {
					mapaPecesElectronicaOperatives.get(peçaRebuda.getPeça_ID()).add(peçaRebuda);
				} else {
					LinkedList<Peça_electronica_Dades> newList = new LinkedList<>();
					newList.add(peçaRebuda);
					mapaPecesElectronicaOperatives.put(peçaRebuda.getPeça_ID(), newList);
				}
			}
			semaphore4Entry.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	public void veureContingutDelMagatzem() {
		System.out.println("Contingut del magatzem: ");
		for (var entry : mapaPecesElectronicaTrencades.entrySet()) {
			System.out.println("\t" + entry.getKey() + ": " + entry.getValue().size());
		}
		for (var entry : mapaPecesElectronicaOperatives.entrySet()) {
			System.out.println("\t" + entry.getKey() + ": " + entry.getValue().size());
		}
	}
	
}
