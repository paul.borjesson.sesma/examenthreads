package com.company;

import java.util.*;
import java.util.Map.Entry;



public class Drassana_Dades implements Runnable {
	public static final String nomDrassana = "MCRN Calisto ";
	private Magatzem_peces_Dades magatzem;

	public Drassana_Dades(Magatzem_peces_Dades magatzem) {
		this.magatzem = magatzem;
	}

	private LinkedList<Nau_Dades> llistaNausEnDrassana = new LinkedList<Nau_Dades>();		// Cua de tipus FIFO que suporti null's
	
	// La clau serà peça_ID. El valor serà la quantitat de peces que hi ha del propi model.
	private LinkedHashMap<String, Integer> mapaStockPeces = new LinkedHashMap<String, Integer>();
	
	// La clau serà peça_ID. El valor seran els objecte de tipus Peça_electronica_Dades i es diferenciaran pel peça_num_serie.
	private LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica = new LinkedHashMap<String, LinkedList<Peça_electronica_Dades>>();
	
	
	

	public LinkedList<Nau_Dades> getLlistaNausEnDrassana() {
		return llistaNausEnDrassana;
	}

	public void setLlistaNausEnDrassana(LinkedList<Nau_Dades> llistaNausEnDrassana) {
		this.llistaNausEnDrassana = llistaNausEnDrassana;
	}

	public LinkedHashMap<String, Integer> getMapaStockPeces() {
		return mapaStockPeces;
	}

	public void setMapaStockPeces(LinkedHashMap<String, Integer> mapaStockPeces) {
		this.mapaStockPeces = mapaStockPeces;
	}

	public LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronica() {
		return mapaPecesElectronica;
	}

	public void setMapaPecesElectronica(LinkedHashMap<String, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica) {
		this.mapaPecesElectronica = mapaPecesElectronica;
	}

	
	public void veureContingutMapaPecesElectronica() {
		if (mapaStockPeces.isEmpty()) {
			System.out.println(nomDrassana + " : FI peces d'electrònica de la drassana:");
			System.out.println("\tmapa buit");
		} else {
			System.out.println(nomDrassana + " : INICI peces d'electrònica de la drassana:");
			for (var peça : mapaStockPeces.entrySet()) {
				System.out.println("\t" + peça.getKey() + ": " + peça.getValue());
			}
		}
	}

	public Magatzem_peces_Dades getMagatzem() {
		return magatzem;
	}

	@Override
	public void run() {
		//1
		System.out.println(nomDrassana + " : INICI " + magatzem.getNausAcabades().toString());
		//2
		veureContingutMapaPecesElectronica();
		System.out.println("---------------------------------------------------------------------");
		//3
		for (var entry : mapaPecesElectronica.entrySet()) {
			ListIterator<Peça_electronica_Dades> it2 = entry.getValue().listIterator();
			while (it2.hasNext()) {
				magatzem.processarPecesElectronica(it2.next());
				it2.remove();
			}
			mapaStockPeces.remove(entry.getKey());			//Quan ha esborrat tots els items de la linkedlist, va a mapaStockPeces i l'esborra l'entrada amb aquella key
			//Al mapa quedarà la key y un linkedlist buit tal i com tu m'has dit
			//Si no faria:
			//mapaPecesElectronica.remove(entry);
		}
		//4
		boolean check = false;
		while (!check) {
			System.out.println(nomDrassana + " : " + magatzem.getNausAcabades().toString());
			if (!magatzem.getNausAcabades().contains(false)) {	//Si no hi ha cap false, s'acaba perquè tots són true
				System.out.println("---------------------------------------------------------------------");
				check = true;
			} else {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		//5
		System.out.println(nomDrassana + " : FI " + magatzem.getNausAcabades().toString());
		veureContingutMapaPecesElectronica();
		System.out.println("---------------------------------------------------------------------");
	}
}
