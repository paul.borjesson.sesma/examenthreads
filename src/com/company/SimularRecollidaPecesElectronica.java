package com.company;



public class SimularRecollidaPecesElectronica {

	public static void main(String[] args) {
		System.out.println("MAIN: INICI");
		System.out.println("---------------------------------------------------------------------");

		Magatzem_peces_Dades magatzem = new Magatzem_peces_Dades();

		Drassana_Dades drassana_dades = new Drassana_Dades(magatzem);

		Drassana.inicialitzarNaus(drassana_dades);
		Drassana.inicialitzarPecesNaus(drassana_dades);
		Drassana.inicialitzarPecesDrassana(drassana_dades);

		Drassana.inicialitzarNausAcabades(drassana_dades);

		Thread drassanaThread = new Thread(drassana_dades);
		Thread[] naus = new Thread[drassana_dades.getLlistaNausEnDrassana().size()];

		for (int i = 0; i < drassana_dades.getLlistaNausEnDrassana().size(); i++) {
			naus[i] = new Thread(drassana_dades.getLlistaNausEnDrassana().get(i));
			naus[i].start();
		}

		drassanaThread.start();

		try {
			drassanaThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		magatzem.veureContingutDelMagatzem();

		System.out.println("MAIN: FI");
	}

}
